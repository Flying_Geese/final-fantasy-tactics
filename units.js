// the clock has to stop when a turn is active until it completes
allUnits = [];
class Unit {
  constructor(name) {
    this.name = name;
    this.hp = 100;
    this.mp = 50;
    this.strength = 30;
    this.defense = 5;
    this.speed = 5;
    this.ct = 0;
    this.status = 'alive';
    this.job = 'Squire';
    this.spriteDir = 'sprites/squires/normal/squire-n-male.gif';
    this.abilities = [];
    this.equipment = {
      leftH: null,
      rightH: null,
      helm: null,
      armor: null,
      misc: null,
    }
    updateScreen(this);
  }

  attack(target) {
    let damage = this.strength - target.defense;
    target.hp = (target.hp - damage);
    alert(`${target.name} lost ${damage} HP`);
    checkStatus(target);
  }

  useSkill(ability, target) {
    var possible = ability.canUse(this);
    if (possible == true) {
      alert(`${this.name} used ${ability.name}`);
      // would depened on ability type/discipline (missile, punch, rain)
      let dmg = ability['params']['damage'];
      target.hp = (target.hp - dmg);
      checkStatus(target);
      console.log(`${target.name} was hit by ${ability.name}!\n(-${dmg} HP)`);
    } else {
      console.log('You can\'t do that.');
    }
  }

  changeJob(newJob) {
    this.job = newJob.constructor.name;
    this.hp = newJob.hp;
    this.strength = newJob.strength;
    this.defense = newJob.defense;
    this.speed = newJob.speed;
    this.mp = newJob.mp;
    this.spriteDir = newJob.spriteDir;
    updateUnit(this);
  }

  equipItem(item) {
    // you should select the part to equip
    // only required for hands
    if (item.type == 'hand') {

      if (this.equipment.leftH == null) {
        this.equipment.leftH = item;
      } else {
        // should check if this is a weapon
        this.equipment.rightH = item;
      }
    } else {
      console.log('Equip', item.name, 'failed');
    }
  }

}

class Archer extends Unit {
  constructor() {
    super();
    this.mp -= 25;
    this.strength -= 3;
    this.defense += 10;
  }
}

class BlackMage extends Unit {
   constructor() {
    super();
    this.hp -= 40;
    this.mp += 80;
    this.strength -= 25;
    this.defense -= 5;
    this.spriteDir = 'sprites/bmage/bmage-n-male.gif';
  }
}

class Knight extends Unit {
   constructor() {
    super();
    this.hp += 50;
    this.mp -= 40;
    this.strength += 10;
    this.defense += 15;
    this.spriteDir = 'sprites/knights/normal/knight-n-male.gif';
  }
}

class Thief extends Unit {
  constructor() {
    super();
    this.hp -= 25;
    this.strength -= 10;
    this.speed += 2;
    this.spriteDir = 'http://www.videogamesprites.net/FinalFantasyTactics/Jobs/Thief/Thief1M-SW.gif';
  }
}


function generate() {
  p1 = new Unit('Abel');
  p2 = new Unit('Wilham');
  // var because you can use it outside of the loop, but not outside the function
  // for (var characterCount = 2; characterCount == 0; characterCount--) {
  //     updateScreen(p1);
  // }
  var characterCount = 2;
  let x = new Thief()
  p1.changeJob(x);
  allUnits.push(p1, p2);
  return `${characterCount} characters generated`;
}

function updateScreen(character) {

  // let polygon = document.createElement('div');
  if (character.name != undefined) {
    var u = doesUnitExist(character);
    if (u == false) {

    let title = document.createElement('h1');
    let hp = document.createElement('p');
    let mp = document.createElement('p');
    let img = document.createElement('img');

    let charname = document.createTextNode(character.name);
    let charhp = document.createTextNode(character.hp);
    let charmp = document.createTextNode(character.mp);
    img.src = character.spriteDir;
    img.width  = '45';
    img.height = '78';
    img.id = character.name;

    title.appendChild(charname);
    hp.appendChild(charhp);
    mp.appendChild(charmp);

    let x = document.getElementById('characters')
    x.appendChild(title);
    x.appendChild(hp);
    x.appendChild(mp);
    x.appendChild(img);
    } else {
      let z = document.getElementById(character.name);
      // console.log(z);
    }
}
}

function updateStats() {

}

function doesUnitExist(unit) {
  var x = document.getElementById(unit.name);
  if (x == undefined) {
    return false;
  } else {
    return true;
  }
}

function updateUnit(unit) {
  var unit = unit;
  console.log(`${unit.name} changed jobs to ${unit.job}`);
  var img = document.getElementById(unit.name);
  img.src = unit.spriteDir;
}

function criticalorDead(target) {
  var target = target;
  var img = document.getElementById(target.name);
  img.src = target.spriteDir;
}

checkStatus = target => {
  if (target.hp / 2 && target.hp > 0) {
    target['status'] = 'critical';
  }
  if (target.hp <= 0) {
    target['status'] = 'dead';
  }
  if (target.status == 'critical') {
    // don't update sprite if the sprite is already there
    target['spriteDir'] = target.spriteDir.replace('.gif', '') + '-critical.gif';
  }
  if (target.status == 'dead') {
    // might not work if unit goes from standing straight to death
    target['spriteDir'] = target.spriteDir.replace('-critical.gif', '') + '-dead.gif';
  }
  criticalorDead(target);
}

// abilities = {
//   { 'throwStone': 'effect' }
// }
