# Final Fantasy JS (FFJS)

Final Fantasy JS is a project inspired by Final Fantasy Tactics: War of the Lions.
Contributors are welcome, but please review the code as thoroughly as possible beforehand to minimize repeated questions.

Link to original game.


## Game config

_Host the configuration files remotely_  
abilities.json  
equipment.json  

## States


## Stages

all battles take place within a stage.
a stage consists of tiles
tiles have properties

Stage

name
schema = [[0, 2, 2, 0]];

buildStage() {
  this.schema ...
}


Tiles?
