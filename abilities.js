// generate all the abilites based ability class
// pregenerated abilites?
// numbers should be in their own attr

everyAbility = [];

class Ability {
	constructor(name, type, discipline, damage, range, jpCost, mpCost, ctCost, phrase) {
		var a = this;
		a.name = name;
		a.type = type;
    a.discipline = discipline;
		a.incantation = { phrase: phrase };
    a.params = {
      damage: damage,
      range: range,
      jpCost: jpCost,
      mpCost: mpCost,
      ctCost: ctCost,
    };
    a.units = [];
	}

  learnAbility(unit) {
    this.units.push(unit);
    unit.abilities.push(this.name);
    console.log(`${unit.name} learned ${this.name}!`);
  }

  canUse(unit) {
     return unit.abilities.includes(this.name);
  }
}

// discipline (Fundaments, Art of War, Black Magic, etc)

function generateAbilities() {
  // name, type, discipline, damage, range, jpCost, mpCost, ctCost
	let x = new Ability('Throw Stone', 'Action', 'Fundaments');
  x.params.damage = 5;
  x.params.range = 4;
  let y = new Ability('Rush', 'Action', 'Fundaments');
  let z = new Ability('Move +1', 'Movement', 'Fundaments');
  let fire = new Ability('Fire 1', 'Action', 'Black Magic');
  fire.params.damage = 35;
  let fire2 = new Ability('Fire 2', 'Action', 'Black Magic');
	fire2.incantation.phrase = 'Burn!';
  fire.params.damage = 55;
	everyAbility.push(x);
	everyAbility.push(y);
  everyAbility.push(z);
  everyAbility.push(fire);
  everyAbility.push(fire2);
	var count = 5;
  while (count < count.length) {
		let fArea = document.getElementById('formation');
	  let element = document.createElement('h3');
		let node = document.createTextNode(everyAbility[count].name);
		element.appendChild(node);
		fArea.appendChild(element);
		count++;
	}

}



/*

Ability

name
damage
description
range
cost (in JP)
mpCost
ctCost

*/

/*

A player learns abilities by spending Job Points

How does a player have an ability to use?

FLOW
-> job screen, click "skills"
-> list of skills; click to buy skill
-> confirm purchase
-> skill gets added to Unit's allSkills (not necessarily activeSkills)
-> Unit can now use the skill



*/
// generate();
// generateAbilities();
// console.log('List of Units');
// console.table([p1, p2]);
console.log('Availablie Abilities');
console.table(everyAbility);

var throwstone = everyAbility[0];
var fira = everyAbility[4];
