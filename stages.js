/*
all battles take place within a stage.
a stage consists of tiles
tiles have properties/types

is a tile its own object?
or are tilesets built as larger objects and later inserted?
*/

class Stage {
  constructor(name, schema, size) {
    this.name = name;
    this.schema = schema;
    this.size = size;
  }

  buildStage(map) {
    var stage = document.getElementById('stage');
    stage.style.display = 'grid';
    stage.style.gridTemplate = createGrid(stage, map);
    stage.style.gridGap = '10px';

    var outerCount = 0;
    var innerCount = 0;
    // base contains 4 rows
    // each row contains 6
    for (var row in map.base) { // 4 times
       for (var cell in map.base[row]) {
         console.log(map.base[row][cell]);
         var div = document.createElement('div');
        if (map.base[row][cell] == 1) {
          div.style.backgroundColor = 'green';
          div.style.height = '100px';
          div.style.width = '100px';
          div.style.padding = '5px';
        } else if (map.base[row][cell] == 0) {
          div.style.backgroundColor = 'black';
          div.style.height = '100px';
          div.style.width = '100px';
          div.style.padding = '5px';
        } else if (map.base[row][cell] == 3) {
          div.style.backgroundColor = 'lightblue';
          div.style.height = '100px';
          div.style.width = '100px';
          div.style.padding = '5px';
        } else {
          div.style.backgroundColor = 'red';
          div.style.height = '100px';
          div.style.width = '100px';
          div.style.padding = '5px';
        }
        innerCount++;
        stage.appendChild(div);
      }
       }
    }
  }


var map1 = {
  base: [ [1, 0, 1, 1, 1, 1],
          [1, 0, 1, 1, 1, 1],
          [1, 1, 1, 1, 0, 0],
          [1, 1, 1, 1, 0, 0]],
  size: [6,4]
}

var map2 = {
  base: [ [1, 1, 1, 3, 3, 1, 1, 1],
          [3, 1, 1, 3, 3, 1, 1, 1],
          [3, 1, 3, 3, 3, 1, 1, 1],
          [1, 1, 1, 1, 3, 1, 1, 1]],
  size: [8,4]
}

var plains = new Stage('Mandalia Plans', map1['base'], map1['size']);
var woods = new Stage('Sweegy Woods', map2['base'], map2['size']);
// plains.buildStage(map1);
// woods.buildStage(map2);

function createGrid(area, map) {
  var cols = map.size[0];
  var rows = map.size[1];
  var template = '';
  // console.log(cols,rows);
  // make rows
  while (rows > 0) {
    template += '100px '
    rows--;
  }
  template += '/ '
  // make cols
  while (cols > 0) {
    template += '100px ';
    cols--;
  }
  console.log(template);
  return template;
}
