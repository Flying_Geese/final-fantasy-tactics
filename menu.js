var characterMenu = document.getElementById('main-char-menu-modal');
var midmenu = document.getElementById('midmenu');
var bottommenu = document.getElementById('bottommenu');
characterMenu.style.display = 'none';

function showCharacterMenu() {
  characterMenu.style.display = 'block';
  midmenu.classList.add('bg-menu');
  bottommenu.classList.add('bg-menu');
}

function dismissMenu() {
  characterMenu.style.display = 'none';
  midmenu.className = 'status-mid';
  bottommenu.className = 'status-bottom';
}
