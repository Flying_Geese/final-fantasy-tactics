var items = {};

class Item {
  constructor(name, cost) {
    this.name = name;
    this.cost = cost;
  }
}

// should really be a weapon class
class Sword extends Item {
  constructor(name, cost, atk) {
    super(name, cost);
    this.atk = atk;
    this.type = 'hand';
  }
}

function loadItems() {
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      for (weapon in data['weapons']["swords"]) {
        var sword = new Sword(
          weapon, data['weapons']['swords'][weapon]['cost'], data['weapons']['swords'][weapon]['atk']
        );
        items[weapon] = sword;
        console.log(sword);
        console.log(weapon, data['weapons']['swords'][weapon]['cost'], data['weapons']['swords'][weapon]['atk'])
      }
      // console.log(sword);
    }
  };
  request.open("GET", "https://bitbucket.org/Flying_Geese/tactics-data/raw/de542636392ad7db14552cfe81ed7115971e1c9c/weapons.json", true);
  request.send();
}
