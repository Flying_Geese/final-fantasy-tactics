var app = document.getElementById('speech');

function whoSpeaks() {
  speaker = document.getElementById('speaker').innerHTML
  if (speaker == 'Agrias') {
    var say = 'Life is short! Bury! Steady sword!';
  } else if (speaker == 'Gafgarion') {
    var say = 'Master of all swords! Cut energy! Night Sword!';
  } else {
    var say = 'Mayhap bowed heads would less offend?';
  }
  return say;
}

var typewriter = new Typewriter(speech, {
    loop: false,
    animateCursor: false,
    typingSpeed: 40,
    cursor: ''
});

typewriter.typeString(whoSpeaks())
    .pauseFor(2500)
    .start();
